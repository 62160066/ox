/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.ox;

import java.util.Scanner;

/**
 *
 * @author W.Home
 */
public class OX {
    public static void main(String[] args) {
        String arr[][] = new String [3][3];
        Scanner kb =  new Scanner(System.in);
        int i,j,turn;
        int row,col;
        boolean draw = true;
        boolean repeat = false;
        turn = 1;       
        for(i = 0;i < 3;i++){
            for(j = 0;j < 3;j++){
                arr[i][j] = "-";
            }      
        }
        System.out.println("Welcome to Ox Gane");
        System.out.println("  1 2 3");
        for(i = 0;i < 3;i++){
            System.out.print((i+1)+" ");        
            for(j = 0;j < 3;j++){
                System.out.print(arr[i][j]+" ");
            }
            System.out.println("");
                    
        }
        while(turn <= 9){
            if(turn % 2 != 0){
                System.out.println("X turn");
                
                do{
                    System.out.println("Please input Row Col : ");
                    row = kb.nextInt();
                    col = kb.nextInt();
                    if(arr[row-1][col-1].equals("-")){
                        arr[row-1][col-1] = "X";
                        repeat = false;
                    }else{
                        System.out.println("Position is not empty ");
                        System.out.println("Please choose other position ");
                        repeat = true;
                    }
                
                }while(repeat == true);
                
            }else{
                System.out.println("O turn");
                do{
                    System.out.println("Please input Row Col : ");
                    row = kb.nextInt();
                    col = kb.nextInt();
                    
                    if(arr[row-1][col-1].equals("-")){
                        arr[row-1][col-1] = "O";
                        repeat = false;
                    }else{
                        System.out.println("Position is not empty ");
                        System.out.println("Please choose other position ");
                        repeat = true;
                    }
                
                }while(repeat == true);              
            }
            System.out.println("  1 2 3");
            for(i = 0;i < 3;i++){
            System.out.print((i+1)+" ");        
                for(j = 0;j < 3;j++){
                    System.out.print(arr[i][j]+" ");
                }
                 System.out.println("");
                    
            }
            //non
            if(arr[0][0].equals(arr[0][1]) && arr[0][1].equals(arr[0][2])&&!arr[0][0].equals("-") ){
                draw = false;
                System.out.println("Player "+arr[0][0]+" Win...");
                System.out.println("Bye bye ...");
                break;
                
            }else if(arr[1][0].equals(arr[1][1]) && arr[1][1].equals(arr[1][2])&&!arr[1][0].equals("-")){
                draw = false;
                System.out.println("Player "+arr[1][1]+" Win...");
                System.out.println("Bye bye ...");
                break;
            }else if(arr[2][0].equals(arr[2][1]) && arr[2][1].equals(arr[2][2])&&!arr[2][0].equals("-")){
                draw = false;
                System.out.println("Player "+arr[2][0]+" Win...");
                System.out.println("Bye bye ...");
                break;    
            }else if(arr[0][0].equals(arr[1][0]) && arr[1][0].equals(arr[2][0])&&!arr[0][0].equals("-")){
                draw = false;
                System.out.println("Player "+arr[0][0]+" Win...");
                System.out.println("Bye bye ...");
                break;
            }else if(arr[0][1].equals(arr[1][1]) && arr[1][1].equals(arr[2][1])&&!arr[0][1].equals("-")){
                draw = false;
                System.out.println("Player "+arr[1][1]+" Win...");
                System.out.println("Bye bye ...");
                break;
            }else if(arr[0][2].equals(arr[1][2]) && arr[1][2].equals(arr[2][2])&&!arr[0][2].equals("-")){
                draw = false;
                System.out.println("Player "+arr[1][2]+" Win...");
                System.out.println("Bye bye ...");
                break;
            }else if(arr[0][0].equals(arr[1][1]) && arr[1][1].equals(arr[2][2])&&!arr[0][0].equals("-")){
                draw = false;
                System.out.println("Player "+arr[1][1]+" Win...");
                System.out.println("Bye bye ...");
                break;
            }else if(arr[0][2].equals(arr[1][1]) && arr[1][1].equals(arr[2][0])&&!arr[1][1].equals("-")){
                draw = false;
                System.out.println("Player "+arr[1][1]+" Win...");
                System.out.println("Bye bye ...");
                break;
            }
           turn++;
        }
       if(draw == true){
           System.out.println("Draw ...");
           System.out.println("Bye bye ...");
       } 
    }
}
